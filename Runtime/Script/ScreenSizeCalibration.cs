using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

namespace ScreenMonitorSize
{
    [RequireComponent(typeof(RawImage))]
    public class ScreenSizeCalibration : MonoBehaviour
    {
        [Header("Scene references")]
        [SerializeField] private Slider _slider;
        [SerializeField] private TMP_InputField _inputMonitorSize;
        [SerializeField] private TextMeshProUGUI _monitorSizeTxt;
        private RawImage _image;

        [Header("Settings")]
        private const float _cardinch = 3.4f; //inches, width of credit card
        private const int _distance = 50; //cm, chosen distance from screen as this approximates to an arm's length
        private float _pxDiagonal; //get the screen's diagonal size in pixels
        private const float _widthPixelOfCard = 384f;
        private const float _heightPixelOfCard = 242f;
        private const float _cardRatio = _widthPixelOfCard / _heightPixelOfCard; //this will store the card's ratio of width to height
        private const float _screenMin = 10; //minimum screen size allowed for the task
        private const float _screenMax = 40; //maximum screen size allowed for the task
        private float _inches; //convert the slider value to inches
        private float _pxperinch; //calculate pixels per inch
        private float _cardWidth; //width of the card (px)
        private float _cardHeight; // height of the card (px)
        private float _pxperdeg;
        public float PxPerDeg { get => _pxperdeg; }

        private float _monitorSize;
        public float MonitorSize { get => _monitorSize; }

        private UnityAction _onValueChanged;
        public UnityAction OnMonitorSizeChanged { get => _onValueChanged; set => _onValueChanged = value; }

        void Awake()
        {
            _image = GetComponent<RawImage>();

            _monitorSizeTxt.text = "?";

            //get the screen's diagonal size in pixels
            _pxDiagonal = Mathf.Sqrt(Mathf.Pow(Screen.width, 2) + Mathf.Pow(Screen.height, 2));

            //slider parameters for changing the displayed object's size
            //the units are inches * 10 (the * 10 helps to elongate the slider's appearances)
            _slider.minValue = _screenMin * 10;
            _slider.maxValue = _screenMax * 10;

            _slider.onValueChanged.AddListener(delegate { ComputeDisplaySize(); });
            _inputMonitorSize.onValueChanged.AddListener(delegate { SetMonitorSize(); });

           
        }

  
        private void ComputeDisplaySize()
        {
            _inches = GetSliderValue() / 10; //convert the slider value to inches
            _pxperinch = _pxDiagonal / _inches; //calculate pixels per inch

            //update the card size based on the calculated pixels per inch
            _cardWidth = Mathf.Round(_pxperinch * _cardinch);
            _cardHeight = Mathf.Round(_cardWidth / _cardRatio);

            _image.rectTransform.sizeDelta = new Vector2(_cardWidth, _cardHeight);

            GetConversion();
        }


        // This function calculates pixels per degrees, based on the size of the monitor
        private void GetConversion()
        {
            //calculate pixels per degree
            float angle = Mathf.Atan(Screen.height / Screen.width);
            float diagCM = (GetSliderValue() / 10) * 2.54f;
            float screenWidthCM = diagCM * Mathf.Cos(angle);
            _pxperdeg = Mathf.PI / 180 * Screen.width * _distance / screenWidthCM;

            //get the final confirmed monitor size
            _monitorSize = (GetSliderValue() / 10);
            _monitorSizeTxt.text = _monitorSize.ToString();

            if(_onValueChanged != null)
                _onValueChanged.Invoke();
        }

        private void SetMonitorSize()
        {
            float inputDiagonal = float.Parse(_inputMonitorSize.text);

            if (inputDiagonal < _screenMin || inputDiagonal > _screenMax)
                return;

            _pxperinch = _pxDiagonal / inputDiagonal;
            _slider.value = _slider.maxValue - inputDiagonal * 10 + _slider.minValue;
        }

        private float GetSliderValue()
        {
            return _slider.maxValue - _slider.value + _slider.minValue;
        }
    }
}
